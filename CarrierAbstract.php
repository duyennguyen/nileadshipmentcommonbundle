<?php
/**
 * Created by Rubikin Team.
 * Date: 9/3/13
 * Time: 10:53 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client as HttpClient;
use Nilead\SettingComponent\Manager\SettingManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Nilead\ShipmentCommonComponent\Message\RequestInterface;
use Nilead\ShipmentComponent\Model\ShippingSubjectInterface;
use Nilead\ShipmentComponent\Carrier\CarrierInterface;

abstract class CarrierAbstract extends ContainerAware implements ContainerAwareInterface, CarrierInterface
{
    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $httpRequest;

    /**
     * @var ParameterBag
     */
    protected $parameters;

    /**
     * @var RequestInterface[]
     */
    protected $requests;

    /**
     * @param ClientInterface $httpClient
     * @param HttpRequest     $httpRequest
     */
    public function __construct(ClientInterface $httpClient = null, HttpRequest $httpRequest = null)
    {
        $this->httpClient = $httpClient ?: $this->getDefaultHttpClient();
        $this->httpRequest = $httpRequest ?: $this->getDefaultHttpRequest();
        $this->initialize();
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(array $parameters = array())
    {
        $this->parameters = new ParameterBag();
        // set default parameters
        foreach ($this->getDefaultParameters() as $key => $value) {
            if (isset($parameters[$key])) {
                $value = $parameters[$key];
            }

            if (is_array($value)) {
                $this->parameters->set($key, reset($value));
            } else {
                $this->parameters->set($key, $value);
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function registerRequest($name, $object)
    {
        $this->requests[$name] = $object;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRequest($name)
    {
        return isset($this->requests[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function request($action, ShippingSubjectInterface $shippingSubject, array $parameters = array())
    {
        /** @var \Nilead\ShipmentCommonComponent\Message\RequestInterface $obj */
        $obj = $this->container->get($this->requests[$action]);

        return $obj->setServices($this->httpClient, $this->httpRequest)->initialize(
            $shippingSubject,
            array_replace($this->parameters->all(), $parameters)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function requestRates(ShippingSubjectInterface $shippingSubject, array $parameters = array())
    {
        return $this->request('rates', $shippingSubject, $parameters);
    }

    // TODO: move to factory to remove dependency on settings bundle
    /**
     * {@inheritdoc}
     */
    public function setSettings(SettingManagerInterface $SettingManager)
    {
        $namespace = 'shipments_carrier_' . $this->getCode();

        if ($SettingManager->hasSettings(
            $namespace
        )
        ) {//var_dump($SettingManager->loadSettings($namespace, true)->all());die('aa');
            $this->setParameters($SettingManager->loadSettings($namespace, true)->all());

        }
    }

    /**
     * {@inheritdoc}
     */
    public function setParameters(array $parameters = array())
    {
        foreach ($parameters as $key => $value) {
            $this->parameters->set($key, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultParameters()
    {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return strtoupper($this->parameters->get('currency'));
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrency($value)
    {
        $this->parameters->set('currency', $value);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters()
    {
        return $this->parameters->all();
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter($key)
    {
        return $this->parameters->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($key, $value)
    {
        $this->parameters->set($key, $value);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTestMode()
    {
        return $this->getParameter('testMode');
    }

    /**
     * {@inheritdoc}
     */
    public function setTestMode($value)
    {
        return $this->setParameter('testMode', $value);
    }

    protected function getDefaultHttpClient()
    {
        return new HttpClient(
            '',
            array(
                'curl.options' => array(CURLOPT_CONNECTTIMEOUT => 60),
            )
        );
    }

    protected function getDefaultHttpRequest()
    {
        return HttpRequest::createFromGlobals();
    }
}
