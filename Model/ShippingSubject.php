<?php
/**
 * Created by Rubikin Team.
 * Date: 5/26/14
 * Time: 12:34 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Model;

use Nilead\ShipmentComponent\Model\ShippingSubjectInterface;
use Nilead\ShipmentComponent\Model\ShippingMethodInterface;

abstract class ShippingSubject implements ShippingSubjectInterface
{
    /**
     * @var float
     */
    protected $weight;

    /**
     * @var float
     */
    protected $weightUnit;

    /**
     * @var float
     */
    protected $length;

    /**
     * @var float
     */
    protected $width;

    /**
     * @var float
     */
    protected $height;

    /**
     * @var
     */
    protected $lengthUnit;

    /**
     * @var ShippingMethodInterface
     */
    protected $shippingMethod;

    /**
     * {@inheritdoc}
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * {@inheritdoc}
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setWeightUnit($weightUnit)
    {
        $this->weightUnit = $weightUnit;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getWeightUnit()
    {
        return $this->weightUnit;
    }

    /**
     * {@inheritdoc}
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * {@inheritdoc}
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * {@inheritdoc}
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * {@inheritdoc}
     */
    public function setLengthUnit($lengthUnit)
    {
        return $this->lengthUnit = $lengthUnit;
    }

    /**
     * {@inheritdoc}
     */
    public function getLengthUnit()
    {
        return $this->lengthUnit;
    }

    /**
     * {@inheritdoc}
     */
    public function setShippingMethod(ShippingMethodInterface $shippingMethod = null)
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }
}
